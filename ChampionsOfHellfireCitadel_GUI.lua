﻿local _L = CHC_Localization;
function CHC_createMainControls()
    local CHC_Frame = CreateFrame("Frame", "ChampoinsOfHellfireCitadel", UIParent);
    CHC_Frame:SetPoint("CENTER", UIParent);
    CHC_Frame:SetMovable(true);
    CHC_Frame:SetUserPlaced(true);
    CHC_Frame:EnableMouse(true);
    CHC_Frame:SetScript("OnMouseDown", function(self, button)
        if button == "LeftButton" and not self.isMoving then
            self:StartMoving();
            self.isMoving = true;
        end;
    end);
    CHC_Frame:SetScript("OnMouseUp", function(self, button)
        if button == "LeftButton" and self.isMoving then
            self:StopMovingOrSizing();
            self.isMoving = false;
        end;
    end);
    CHC_Frame:SetScript("OnHide", function(self)
        if self.isMoving then
            self:StopMovingOrSizing();
            self.isMoving = false;
        end;
    end);
    CHC_Frame:SetFrameStrata("BACKGROUND");
    CHC_Frame:SetHeight(30);
    CHC_Frame:SetWidth(210);
    CHC_Frame:SetBackdrop({bgFile = "Interface/Tooltips/UI-Tooltip-Background",
        edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
        tile = true, tileSize = 3, edgeSize = 10, 
        insets = { left = 1, right = 1, top = 1, bottom = 1 }
    });
    CHC_Frame:SetBackdropColor(0.9, 0.9, 0.9, 0.6);
    CHC_Frame.text = CHC_Frame:CreateFontString(nil, "ARTWORK");
    CHC_Frame.text:SetFont("Fonts\\FRIZQT__.TTF", 14);
    CHC_Frame.text:SetTextColor(1, 1, 1);
    CHC_Frame.text:SetAllPoints();
    
    local CHC_NPCLabel = CreateFrame("Frame", "CHC_NPCLabel", CHC_Frame);
    CHC_NPCLabel:SetFrameStrata("BACKGROUND");
    CHC_NPCLabel:SetPoint("BOTTOMLEFT", CHC_Frame, 5, 5);
    CHC_NPCLabel:SetWidth(120);
    CHC_NPCLabel:SetHeight(20);
    CHC_NPCLabel.text = CHC_NPCLabel:CreateFontString(nil, "ARTWORK");
    CHC_NPCLabel.text:SetFont("Fonts\\ARIALN.TTF", 14);
    CHC_NPCLabel.text:SetTextColor(1, 1, 1);
    CHC_NPCLabel.text:SetText("");
    CHC_NPCLabel.text:SetAllPoints();
    
    local CHC_SearchButton = CreateFrame("Button", "SearchButton", CHC_Frame, "UIPanelButtonTemplate");
    CHC_SearchButton:SetPoint("BOTTOMRIGHT", CHC_Frame, -5, 5);
    CHC_SearchButton:SetWidth(75);
    CHC_SearchButton:SetHeight(20);
    CHC_SearchButton:SetText(_L.SEARCH);
    CHC_SearchButton:SetNormalFontObject("GameFontNormal");
    
    return CHC_Frame, CHC_NPCLabel, CHC_SearchButton;
end;