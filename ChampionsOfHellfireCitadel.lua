local addon = ...;
local _L = CHC_Localization;
local ingame_meter = 0.00018875;
local PlayerData = {
    ['Position'] = {
        ['X'] = 0;
        ['Y'] = 0;
    };
    ['inTanaan'] = false;
    ['inNPCZone'] = false;
    ['currentNPCID'] = 0;
    ['isSearching'] = false;
    ['inCombat'] = false;
    ['level'] = 0;
    ['name'] = 0;
    ['updateEnabled'] = true;
};
local CHC_Settings;
local CHC_Defaults = {
    ['DetectMeters'] = 250;
    ['MinimapPosition'] = 45;
    ['MaxAge'] = 30;
    ['SearchDelay'] = 3;
    ['UpdateDelay'] = 0.5;
    ['ShowMinimapButton'] = true;
    ['Debug'] = false;
};
local CHC_ticker = nil;
local ticker_counter = 0;
local ticksPerSearchQuery = 6;
local CHC_Frame, CHC_NPCLabel, CHC_SearchButton;
local blacklist = {};
local waitingInvite, inPartyForRare = false, false;

----------------- Rare NPC data ------------------
local NPCData = {
    ['95053'] = { QuestId = 39287, Completed = false, Point = { X = 0.23284393548965, Y = 0.39656138420105 }, SearchKeywords = {'death'} };
    ['95044'] = { QuestId = 39288, Completed = false, Point = { X = 0.14002251625061, Y = 0.60541999340057 }, SearchKeywords = {'terror'} };
    ['95056'] = { QuestId = 39289, Completed = false, Point = { X = 0.46980005502701, Y = 0.52627778053284 }, SearchKeywords = {'doom'} };
    ['95054'] = { QuestId = 39290, Completed = false, Point = { X = 0.32523393630981, Y = 0.73982304334641 }, SearchKeywords = {'veng'} };
};
--------------------------------------------------

------------------ Functions ---------------------

local function CHC_Dump(t)
    if type(t) == 'table' then
        local s = '{ ';
        for k, v in pairs(t) do
            if type(k) ~= 'number' then k = '"' .. k .. '"' end;
            s = s .. '[' .. k .. '] = ' .. CHC_Dump(v) .. ',';
        end;
        return s .. '} ';
    else
        return tostring(t);
    end;
end;

local function CHC_Print(text, ...)
    local r, g, b = ...;
    r = r or 0.7;
    g = g or 1.0;
    b = b or 0.7;
    DEFAULT_CHAT_FRAME:AddMessage(' \124cff009933<ChampionsOfHellfireCitadel>\124r ' .. tostring(text), r, g, b);
end;

local function CHC_Debug(var, info)
    if CHC_Settings
    then
        if CHC_Settings.Debug ~= true then return; end;
        local var = var or 'not set';
        local info = info or 'CHC_Debug';
        CHC_Print(tostring(info) .. ': ' .. CHC_Dump(var), 1, 0, 0);
    end;
end;

local function CHC_Distance(x1, y1, x2, y2)
    local distance = math.sqrt((x2 - x1) ^ 2 + (y2 - y1) ^ 2);
    return distance;
end;

local function CHC_getSettings()
    ChampionsOfHellfireCitadel_Settings = ChampionsOfHellfireCitadel_Settings or {};
    CHC_Settings = ChampionsOfHellfireCitadel_Settings;
    for var, value in pairs(CHC_Defaults) do
        CHC_Settings[var] = CHC_Settings[var] == nil and value or CHC_Settings[var];
    end;
    CHC_Print('settings loaded');
    CHC_Debug(CHC_Settings, 'CHC_getSettings');
end;


function CHC_setOption(name, value)
    local optionKey = '';
    if string.match(name, "CHC_Opt_") then
        optionKey = string.gsub(name, 'CHC_Opt_', '');
        CHC_Settings[optionKey] = value;
        CHC_Debug(optionKey .. ' ' .. tostring(value), 'change settings');
        CHC_setOptions();
    end;
end;

function CHC_optionsOnLoad(panel)
    panel.name = GetAddOnMetadata("ChampionsOfHellfireCitadel", "Title");
	InterfaceOptions_AddCategory(panel);
end;

function CHC_optionsOnShow()
    CHC_AddonName:SetText(GetAddOnMetadata("ChampionsOfHellfireCitadel", "Title") .. " v." .. GetAddOnMetadata("ChampionsOfHellfireCitadel", "Version"));
    local thanks, author, showminimap, sd, ud, ma, ds, dm = _L.THANKS or 'Special thanks to', _L.AUTHOR or 'Author', _L.SHOW_MINIMAP_BUTTON or 'Show minimap button', _L.SEARCH_DELAY or 'Search delay', _L.UPDATE_DELAY or 'Update delay', _L.MAX_AGE or 'Results max age', _L.DEFAULT_SETTINGS or 'Default settings', _L.RADIUS_OF_DETECTING or 'Radius of detecting';
    CHC_Thanks:SetText(thanks .. ': pas06, Illisilien, Acp1571.');
    CHC_Author:SetText(author .. ': Ярославче @ Ревущий Фьорд');
    if CHC_Opt_ShowMinimapButtonText then CHC_Opt_ShowMinimapButtonText:SetText(showminimap); end;
    CHC_Opt_SearchDelayLayer:SetText(sd);
    CHC_Opt_UpdateDelayLayer:SetText(ud);
    CHC_Opt_MaxAgeLayer:SetText(ma);
    CHC_Opt_DetectMetersLayer:SetText(dm);
    CHC_SetDefault:SetText(ds);
    CHC_Opt_SearchDelay:SetValue(CHC_Settings.SearchDelay);
    CHC_Opt_UpdateDelay:SetValue(CHC_Settings.UpdateDelay);
    CHC_Opt_MaxAge:SetValue(CHC_Settings.MaxAge);
    CHC_Opt_DetectMeters:SetValue(CHC_Settings.DetectMeters);
    CHC_Opt_ShowMinimapButton:SetChecked(CHC_Settings.ShowMinimapButton);
end;

function CHC_validateSettings()
    local updateDelay = tonumber(CHC_Settings.UpdateDelay);
    if updateDelay < 0.5 then updateDelay = 0.5;
    elseif updateDelay > 3 then updateDelay = 3; end;
    CHC_Settings.UpdateDelay = updateDelay;
    local searchDelay = tonumber(CHC_Settings.SearchDelay);
    if searchDelay < 1 then searchDelay = 1;
    elseif searchDelay > 10 then searchDelay = 10; end;
    CHC_Settings.SearchDelay = searchDelay;
    local maxAge = tonumber(CHC_Settings.MaxAge);
    if maxAge < 15 then maxAge = 15;
    elseif maxAge > 60 then maxAge = 60; end;
    CHC_Settings.MaxAge = maxAge;
    if searchDelay < updateDelay then searchDelay = updateDelay; end;
    ticksPerSearchQuery = math.floor(searchDelay/updateDelay + 0.5);
end;

local function CHC_minimapButton_Reposition()
    CHC_MinimapButton:SetPoint("TOPLEFT", "Minimap", "TOPLEFT", 52 - (80 * cos(CHC_Settings.MinimapPosition)), (80 * sin(CHC_Settings.MinimapPosition)) - 52);
end;

local function CHC_getCommonPlayerData()
    PlayerData.level = UnitLevel('player');
    PlayerData.name, _ = UnitName('player');
    CHC_Debug(PlayerData.level .. ' ' .. PlayerData.name, 'CHC_getCommonPlayerData');
end;

local function CHC_inTanaanJungle()
    local areaid = GetCurrentMapAreaID();
    local inTanaan = false;
    if areaid == 945 then inTanaan = true; end;
    PlayerData.inTanaan = inTanaan;
    CHC_Debug(PlayerData.inTanaan, 'CHC_inTanaanJungle');
end;

local function CHC_checkCompleted()
    local allCompleted = true;
    for npcId, npcData in pairs(NPCData) do
        npcData.Completed = IsQuestFlaggedCompleted(npcData.QuestId);
        if npcData.Completed == false then allCompleted = false; end;
    end;
    -- if CHC_Settings.Debug == true then NPCData['95056']['Completed'] = false; allCompleted = false; end;
    if allCompleted == true then PlayerData.updateEnabled = false; end;
    CHC_Debug('checkCompleted');
end;

local function CHC_toggleScanning(...)
    local data, button = ...;
    data = data or '';
    button = button or false;
    if PlayerData.isSearching == false then
        local isLeader, inParty = UnitIsGroupLeader("player"), false;
        local npm, ngm = GetNumGroupMembers() or 0, GetNumSubgroupMembers() or 0;
        if npm > 0 or ngm > 0 then inParty = true; end;
        if inParty == true and isLeader == false then
            CHC_Print(_L.CANT_START_SCANNING .. ' ' .. _L.NOT_LEADER, 1, 0, 0);
            RaidWarningFrame_OnEvent(RaidWarningFrame, "CHAT_MSG_RAID_WARNING", _L.CANT_START_SCANNING .. ' ' .. _L.NOT_LEADER);
            return;
        end;
        CHC_Print(_L.START_SCANNING);
        if PlayerData.inNPCZone == false or PlayerData.currentNPCID == 0 or PlayerData.updateEnabled == false then
            CHC_Print(_L.CANT_START_SCANNING, 1, 0, 0);
            return;
        end;
    else
        CHC_Print(_L.CANCEL_SCANNING);
    end;
    PlayerData.isSearching = not PlayerData.isSearching;
    CHC_SearchButton:SetText(PlayerData.isSearching and _L.CANCEL or _L.SEARCH);
    ticker_counter = 0;
    CHC_Debug('isSearching: ' .. tostring(PlayerData.isSearching), 'CHC_toggleScanning');
end;

local function CHC_checkDistance()
    local inNPCZone, currentNPCID = false, 0;
    for npcId, npcData in pairs(NPCData) do
        local distance = CHC_Distance(PlayerData.Position.X, PlayerData.Position.Y, npcData.Point.X, npcData.Point.Y);
        if distance <= (CHC_Settings.DetectMeters * ingame_meter) and npcData.Completed == false
        then
            inNPCZone = true;
            CHC_NPCLabel.text:SetText(_L['NPC'][npcId]['Name']);
            currentNPCID = npcId;
        end;
    end;
    if PlayerData.isSearching == true and inNPCZone == false then CHC_toggleScanning(); end;
    return inNPCZone, currentNPCID;
end;

local function CHC_updatePlayerPosition()
    local currentZoneIndex, continentIndex = GetCurrentMapZone(), GetCurrentMapContinent();
    SetMapToCurrentZone();
    PlayerData.Position.X, PlayerData.Position.Y = GetPlayerMapPosition("player");
    SetMapZoom(continentIndex, currentZoneIndex);
    PlayerData.inNPCZone, PlayerData.currentNPCID = CHC_checkDistance();
end;

local function CHC_getLanguageFilters()
    local available_languages = C_LFGList.GetAvailableLanguageSearchFilter();
    local langFilters = {};
    for i=1, #available_languages do
        local lang = available_languages[i];
        langFilters[lang] = true;
    end;
    return langFilters;
end;

local function CHC_updateBlacklist()
    local currentTime = time();
    for grpId, grpAddedTime in pairs(blacklist) do
        local left = grpAddedTime + CHC_Settings.MaxAge;
        if left <= currentTime then
            blacklist[grpId] = nil;
        end;
    end;
end;

local function CHC_addToBlacklist(name, age, leaderName)
    CHC_Debug('Add to blacklist: ' .. ' ' .. tostring(name) .. ' ' .. tostring(leaderName));
    local curtime = time();
    local created = curtime - age;
    local created_inc, created_dec = created + 1, created - 1;
    blacklist[tostring(created_inc) .. '_' .. tostring(name) .. '_' .. tostring(leaderName)] = created_inc;
    blacklist[tostring(created) .. '_' .. tostring(name) .. '_' .. tostring(leaderName)] = created;
    blacklist[tostring(created_dec) .. '_' .. tostring(name) .. '_' .. tostring(leaderName)] = created_dec;
end;

local function CHC_inBlacklist(name, age, leaderName)
    local created = time() - age;
    local key = tostring(created) .. '_' .. tostring(name) .. '_' .. tostring(leaderName);
    local inblacklist = false;
    if blacklist[key] ~= nil then inblacklist = true end;
    -- CHC_Print(key .. ' in blacklist: ' .. tostring(inblacklist));
    return inblacklist;
end;

local function CHC_updatePlayerData()
    if PlayerData.inTanaan == true and PlayerData.updateEnabled
    then
        CHC_updatePlayerPosition();
        if PlayerData.isSearching == true then
            CHC_updateBlacklist();
            if PlayerData.inNPCZone == true and PlayerData.currentNPCID ~= 0 then
                if ticker_counter == 0
                then
                    CHC_Debug('SEARCH');
                    C_LFGList.ClearSearchResults();
                    C_LFGList.Search(6, NPCData[PlayerData.currentNPCID]['SearchKeywords'][1], 5, nil, CHC_getLanguageFilters());
                end;
                ticker_counter = ticker_counter + 1;
                if ticker_counter == ticksPerSearchQuery then ticker_counter = 0; end;
            else
                CHC_toggleScanning();
                CHC_Frame:Hide();
            end;
        else
            if PlayerData.inNPCZone == true and PlayerData.currentNPCID ~= 0
            then
                CHC_Frame:Show();
            else
                CHC_Frame:Hide();
            end;
        end;
    else
        if PlayerData.isSearching then CHC_toggleScanning(); end;
        CHC_Frame:Hide();
    end;
end;

local function CHC_getPlayerRoles()
    local currentSpec = GetSpecialization();
    local _, _, _, _, _, role = GetSpecializationInfo(currentSpec);
    local isTank, isHealer, isDamager = false, false, false;
    if role == 'TANK' then isTank = true; end;
    if role == 'HEALER' then isHealer = true; end;
    if role == 'DAMAGER' then isDamager = true; end;
    return isTank, isHealer, isDamager;
end;

local function CHC_collectResults(...)
    if PlayerData.isSearching ~= true or PlayerData.currentNPCID == 0 then return; end;
    local numResults, resultIDTable = C_LFGList.GetSearchResults();
    for k, v in pairs(resultIDTable) do
        local id, activityID, name, comment, voiceChat, iLvl, age, numBNetFriends, numCharFriends, numGuildMates, isDelisted, leaderName, numMembers = C_LFGList.GetSearchResultInfo(v);
        leaderName = leaderName or '';
        if not isDelisted
        then
            local str_match = false;
            for mk, mv in pairs(NPCData[PlayerData.currentNPCID]['SearchKeywords']) do
                if string.match(string.lower(name), mv) then str_match = true; end;
            end;
            if age < CHC_Settings.MaxAge and str_match == true and leaderName ~= '' and CHC_inBlacklist(name, age, leaderName) == false then
                waitingInvite = true;
                CHC_addToBlacklist(name, age, leaderName);
                local t, h, d = CHC_getPlayerRoles();
                C_LFGList.ApplyToGroup(id, "", t, h, d);
                PlaySound("ReadyCheck", "master");
                CHC_toggleScanning();
                return;
            end;
        end;
    end;
end;

local function getNPCDataFromGuid(guid)
    local separator, guidData, keys, dummy = '-', {}, {'unitType', '0', 'serverID', 'instanceID', 'zoneUID', 'unitID', 'spawnUID'}, 1;
    local pattern = string.format("([^%s]+)", separator);
    guid:gsub(pattern, function(c)
        guidData[keys[dummy]] = c;
        dummy = dummy + 1;
    end);
    return guidData;
end;

local function CHC_checkUnitDead(...)
    local timestamp, evtype, hideCaster, sourceGUID, sourceName, sourceFlags, sourceFlags2, destGUID, destName, destFlags, destFlags2 = select(1, ...);
    if evtype == 'UNIT_DIED' then
        local gdata = getNPCDataFromGuid(destGUID);
        if gdata.unitID ~= nil then
            local killedNpcId = gdata.unitID or 0;
            destName = destName or '';
            CHC_Debug('UNIT DEAD ' .. killedNpcId .. ' ' .. destName);
            if NPCData[killedNpcId] then
                if NPCData[killedNpcId]['Completed'] == true then return; end;
                local remaining = '';
                for npcId, npcData in pairs(NPCData) do
                    local d = '';
                    if remaining == '' then d = ''; else d = ', '; end;
                    if npcData.Completed == false then
                        if npcId ~= killedNpcId then remaining = remaining .. d .. CHC_Localization['NPC'][npcId]['Name']; end;
                    end;
                end;
                if remaining ~= '' then
                    RaidWarningFrame_OnEvent(RaidWarningFrame, "CHAT_MSG_RAID_WARNING", _L.REMAINING .. remaining);
                    CHC_Print(_L.REMAINING .. remaining);
                else
                    PlaySound('QUESTCOMPLETED', 'master');
                    RaidWarningFrame_OnEvent(RaidWarningFrame, "CHAT_MSG_RAID_WARNING", _L.TODAY_ALL_COMPLETED);
                    CHC_Print(_L.TODAY_ALL_COMPLETED);
                end;
                CHC_updatePlayerData();
            end;
        end;
    end;
end;

local function CHC_rosterUpdate()
    local npm, ngm = GetNumGroupMembers() or 0, GetNumSubgroupMembers() or 0;
    if npm > 0 or ngm > 0
    then
        if PlayerData.isSearching then CHC_toggleScanning(); end;
        if waitingInvite == true then
            inPartyForRare = true;
            waitingInvite = false;
        end;
    else
        CHC_checkCompleted();
        CHC_updatePlayerData();
        if PlayerData.currentNPCID ~= 0 and inPartyForRare == true then
            if NPCData[PlayerData.currentNPCID]['Completed'] == false then CHC_toggleScanning(); end;
        end;
        inPartyForRare = false;
    end;
end;

function CHC_setOptions()
    CHC_validateSettings();
    if CHC_Settings.ShowMinimapButton == false then CHC_MinimapButton:Hide();
    else CHC_MinimapButton:Show();
    end;
    ticker_counter = 1;
    if ticksPerSearchQuery == 1 then ticker_counter = 0; end;
    if CHC_ticker then CHC_ticker:Cancel(); end;
    CHC_ticker = C_Timer.NewTicker(CHC_Settings.UpdateDelay, CHC_updatePlayerData);
end;

--------------------------------------------------

---------------- GUI and EVENTS ------------------
CHC_Frame, CHC_NPCLabel, CHC_SearchButton = CHC_createMainControls();
CHC_Frame:RegisterEvent("PLAYER_ENTERING_WORLD");
CHC_Frame:RegisterEvent("ZONE_CHANGED");
CHC_Frame:RegisterEvent("ZONE_CHANGED_INDOORS");
CHC_Frame:RegisterEvent("ZONE_CHANGED_NEW_AREA");
CHC_Frame:RegisterEvent("LFG_LIST_SEARCH_RESULTS_RECEIVED");
CHC_Frame:RegisterEvent("LFG_LIST_SEARCH_RESULT_UPDATED");
CHC_Frame:RegisterEvent("RAID_ROSTER_UPDATE");
CHC_Frame:RegisterEvent("GROUP_ROSTER_UPDATE");
CHC_Frame:RegisterEvent("VARIABLES_LOADED");
CHC_Frame:RegisterEvent("PLAYER_REGEN_DISABLED");
CHC_Frame:RegisterEvent("PLAYER_REGEN_ENABLED");
CHC_Frame:RegisterEvent("PARTY_INVITE_REQUEST");
CHC_Frame:SetScript("OnEvent",
    function(self, event, ...)
        if event ~= 'COMBAT_LOG_EVENT_UNFILTERED' and event ~= 'LFG_LIST_SEARCH_RESULT_UPDATED' and event ~= 'LFG_LIST_SEARCH_RESULTS_RECEIVED' then CHC_Debug(event, 'CHC_EVENT_HANDLER'); end;
        if event == "PLAYER_ENTERING_WORLD" then
            CHC_getCommonPlayerData();
            CHC_inTanaanJungle();
            CHC_checkCompleted();
            CHC_updatePlayerData();
        elseif event == "ZONE_CHANGED" or event == "ZONE_CHANGED_INDOORS" or event == "ZONE_CHANGED_NEW_AREA"  then
            CHC_inTanaanJungle();
            CHC_checkCompleted();
            CHC_updatePlayerData();
        elseif event == "LFG_LIST_SEARCH_RESULTS_RECEIVED" or event == "LFG_LIST_SEARCH_RESULT_UPDATED" then
            CHC_collectResults(...);
        elseif event == "RAID_ROSTER_UPDATE" or event == "GROUP_ROSTER_UPDATE" then
            CHC_rosterUpdate();
        elseif event == "VARIABLES_LOADED" then
            CHC_getSettings();
            CHC_minimapButton_Reposition();
            CHC_optionsOnShow();
            CHC_Options:SetScript("OnShow", CHC_optionsOnShow);
            CHC_setOptions();
        elseif event == "COMBAT_LOG_EVENT_UNFILTERED" then
            if PlayerData.inCombat == true then
                CHC_checkUnitDead(...);
            end;
        elseif event == "PLAYER_REGEN_DISABLED" then
            PlayerData.inCombat = true;
            CHC_Frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
        elseif event == "PLAYER_REGEN_ENABLED" then
            PlayerData.inCombat = false;
            CHC_Frame:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
        elseif event == "PARTY_INVITE_REQUEST" then
            -- local a1, a2, a3, a4, a5 = ...;
            -- a1 = a1 or '';
            -- a2 = a2 or '';
            -- a3 = a3 or '';
            -- a4 = a4 or '';
            -- a5 = a5 or '';
            -- CHC_Print('PARTY_INVITE_REQUEST' .. a1 .. ' ' .. a2 .. ' ' .. a3 .. ' ' .. a4 .. ' ' .. a5);
        end;
    end
);

function CHC_checkbuttonLabel(cb)
    local cbname, optionKey = cb:GetName(), '';
    if cbname and cbname ~= '' then
        optionKey = string.gsub(cbname, 'CHC_Opt_', '');
    end;
    if _G[cbname .. 'Text'] then
        local cbtext = _G[cbname .. 'Text'];
        if _L[optionKey] then
            cbtext:SetText(_L[optionKey]);
        else
            cbtext:SetText(optionKey);
        end;
    end;
end;

CHC_SearchButton:SetScript("OnClick", CHC_toggleScanning);

-------------------- SLAHS -----------------------

function slashHandler(param)
    if param == "debug" then
        CHC_Settings.Debug = not CHC_Settings.Debug;
    elseif param == "minimap" then
        CHC_Settings.ShowMinimapButton = not CHC_Settings.ShowMinimapButton;
        if CHC_Settings.ShowMinimapButton == false then CHC_MinimapButton:Hide();
        else CHC_MinimapButton:Show();
        end;
    else
        CHC_Print('Usage:');
        -- CHC_Print(' /chc debug - on/off debug mode (a lot of spam)');
        CHC_Print(' /chc minimap - show/hide minimap button');
    end
end

SLASH_CHC1 = '/chc';
SlashCmdList["CHC"] = function(param, editbox) slashHandler(param); end;

--------------------------------------------------

------------------- Minimap ----------------------
function CHC_MinimapButton_DraggingFrame_OnUpdate()
    local xpos, ypos = GetCursorPosition();
    local xmin, ymin = Minimap:GetLeft(), Minimap:GetBottom();
    xpos = xmin - xpos / UIParent:GetScale() + 70;
    ypos = ypos / UIParent:GetScale() - ymin - 70;
    CHC_Settings.MinimapPosition = math.deg(math.atan2(ypos, xpos));
    CHC_minimapButton_Reposition();
end;

function CHC_MinimapButton_OnClick(self, button, down)
    if self.dragging then return; end;
    local isDown = down or false;
    if button == 'LeftButton' and down == false then
        InterfaceOptionsFrame_OpenToCategory(CHC_Options);
    elseif button == 'RightButton' and down == false then
    end;
end;

function CHC_MinimapButton_OnEnter(self)
    if self.dragging then return; end;
    GameTooltip:SetOwner(self or UIParent, "ANCHOR_LEFT");
    CHC_MinimapButton_Tooltip(GameTooltip);
end;

function CHC_MinimapButton_Tooltip(tt)
    CHC_checkCompleted();
    tt:SetText("Champions of Hellfire Citadel");
    for npcId, npcData in pairs(NPCData) do
        tt:AddLine(format("%s: %s", _L['NPC'][npcId]['Name'], npcData.Completed and "\124cff00ff00" .. _L.COMPLETED .. "\124r" or "\124cffff0000" .. _L.NOT_COMPLETED_YET .. "\124r"), 1, 1, 1);
    end;
    tt:Show();
end;
--------------------------------------------------