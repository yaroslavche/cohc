﻿## Interface: 60200
## Title: Champions of Hellfire Citadel
## Author: Ярославче @ Ревущий Фьорд
## Version: 0.4.2
## SavedVariables: ChampionsOfHellfireCitadel_Settings
## X-Curse-Packaged-Version: r24
## X-Curse-Project-Name: Champions of Hellfire Citadel
## X-Curse-Project-ID: champions-hellfire-citadel
## X-Curse-Repository-ID: wow/champions-hellfire-citadel/mainline

localization/enUS.lua
localization/ruRU.lua
localization/deDE.lua
localization/esES.lua

ChampionsOfHellfireCitadel.xml
ChampionsOfHellfireCitadel_GUI.lua
ChampionsOfHellfireCitadel.lua
ChampionsOfHellfireCitadel_Options.xml