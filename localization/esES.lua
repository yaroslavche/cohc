﻿-- translated by Acp1571
if GetLocale() ~= 'esES' then return end;
CHC_Localization = {
    ['NPC'] = {
        ['95053'] = { ['Name'] = 'Garramuerte'},
        ['95044'] = { ['Name'] = 'Horropuño'},
        ['95056'] = { ['Name'] = 'Fatalitas'},
        ['95054'] = { ['Name'] = 'Venganza'},
    };
    
    ['CANT_START_SCANNING'] = 'No puedes comenzar la búsqueda.';
    
    ['SEARCH'] = 'Buscar';
    ['CANCEL'] = 'Cancelar';
    ['START_SCANNING'] = 'Buscando...';
    ['CANCEL_SCANNING'] = 'Búsqueda terminada.';
    
    ['COMPLETED'] = 'completado';
    ['NOT_COMPLETED_YET'] = 'no completado';
    
    ['TODAY_ALL_COMPLETED'] = 'Buen trabajo, has completado todo por hoy!';
    ['REMAINING'] = 'Raros restantes: ';
    
    ['SHOW_MINIMAP_BUTTON'] = 'Mostrar botón en el minimapa';
    
    ['THANKS'] = 'Un agradecimiento especial a';
    ['AUTHOR'] = 'Autor';
    ['SEARCH_DELAY'] = 'Intervalo de búsqueda';
    ['UPDATE_DELAY'] = 'Actualización de los datos';
    ['MAX_AGE'] = 'Tiempo de creación del grupo';
    ['DEFAULT_SETTINGS'] = 'Configuración predeterminada';
    ['RADIUS_OF_DETECTING'] = 'Radio de detección';
    ['NOT_LEADER'] = 'No eres el líder del grupo.';
    
};