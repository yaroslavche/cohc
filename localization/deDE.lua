﻿-- translated by pas06
if GetLocale() ~= 'deDE' then return end;
CHC_Localization = {
    ['NPC'] = {
        ['95053'] = { ['Name'] = 'Todeskralle'},
        ['95044'] = { ['Name'] = 'Terrorfaust'},
        ['95056'] = { ['Name'] = 'Verdammniswalze'},
        ['95054'] = { ['Name'] = 'Rache'},
    };
    
    ['CANT_START_SCANNING'] = 'Du kannst die Suche nicht starten.';
    
    ['SEARCH'] = 'Suche...';
    ['CANCEL'] = 'Abbrechen';
    ['START_SCANNING'] = 'Suche beginnt';
    ['CANCEL_SCANNING'] = 'Suche abgebrochen';
    
    ['COMPLETED'] = 'abgeschlossen';
    ['NOT_COMPLETED_YET'] = 'noch nicht fertig';
    
    ['TODAY_ALL_COMPLETED'] = 'Glückwunsch, Du hast für heute alles erledigt';
    ['REMAINING'] = 'Verbleibende Kreaturen: ';
    
    ['SHOW_MINIMAP_BUTTON'] = 'Zeige Minikarten-Button';
    
    ['THANKS'] = 'Vielen Dank an';
    ['AUTHOR'] = 'Autor';
    ['SEARCH_DELAY'] = 'Suchverzögerung';
    ['UPDATE_DELAY'] = 'Verzögerung der Datenaktualisierung';
    ['MAX_AGE'] = 'Max. Alter der Ergebnisse';
    ['DEFAULT_SETTINGS'] = 'Standardeinstellungen';
    ['RADIUS_OF_DETECTING'] = 'Erfassungssradius';
    ['NOT_LEADER'] = 'Du bist nicht der Gruppenanführer.';
    
};