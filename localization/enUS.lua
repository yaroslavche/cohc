﻿CHC_Localization = {
    ['NPC'] = {
        ['95053'] = { ['Name'] = 'Deathtalon'},
        ['95044'] = { ['Name'] = 'Terrorfist'},
        ['95056'] = { ['Name'] = 'Doomroller'},
        ['95054'] = { ['Name'] = 'Vengeance'},
    };
    
    ['CANT_START_SCANNING'] = 'You can\'t start scanning.';
    
    ['SEARCH'] = 'Search';
    ['CANCEL'] = 'Cancel';
    ['START_SCANNING'] = 'Start scanning.';
    ['CANCEL_SCANNING'] = 'Scanning complete.';
    
    ['COMPLETED'] = 'completed';
    ['NOT_COMPLETED_YET'] = 'not completed yet';
    
    ['TODAY_ALL_COMPLETED'] = 'GZ, You have completed the task today.';
    ['REMAINING'] = 'Remaining creatures: ';
    
    ['SHOW_MINIMAP_BUTTON'] = 'Show minimap button';
    
    ['THANKS'] = 'Special thanks to';
    ['AUTHOR'] = 'Author';
    ['SEARCH_DELAY'] = 'Search delay';
    ['UPDATE_DELAY'] = 'Data update delay';
    ['MAX_AGE'] = 'Max age of results';
    ['DEFAULT_SETTINGS'] = 'Default settings';
    ['RADIUS_OF_DETECTING'] = 'Radius of detecting';
    ['NOT_LEADER'] = 'You are not a leader of the group.';
    
};